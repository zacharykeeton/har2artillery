const expect = require('chai').expect
const h2aLib = require('../lib/h2aLib')

describe('h2aLib.js', () => {

  describe('mergeConfigs', () => {
  	it('should override config values with argv values', () => {

      // NOTE: have to do it several times (watching the bools)
      // Arrange
      let config = {
        target: 'foo',
        filter: 'foo',
        harFiles: ['foo'],
        includeAllURLs: false,
        outputFileName: 'foo',
        outputFormat: 'foo',
        useHumanTime: false,
        verbosity: 0
      }

      let argv = {
        target: 'bar',
        filter: 'bar',
        files: ['bar'],
        all: true,
        output: 'bar',
        format: 'bar',
        humantime: true,
        verbose: 1
      }

      // Act
      let finalConfig = h2aLib.mergeConfigs(config, argv)

      // Assert
      expect(finalConfig).to.deep.equal({
        target: 'bar',
        filter: 'bar',
        harFiles: ['bar'],
        includeAllURLs: true,
        outputFileName: 'bar',
        outputFormat: 'bar',
        useHumanTime: true,
        verbosity: 1
      });

      ///////////////////////////
      // Arrange
      config = {
        target: 'bar',
        filter: 'bar',
        harFiles: ['bar'],
        includeAllURLs: false,
        outputFileName: 'bar',
        outputFormat: 'bar',
        useHumanTime: false,
        verbosity: 1
      }

      argv = {
        target: 'foo',
        filter: 'foo',
        files: ['foo'],
        all: undefined,
        output: 'foo',
        format: 'foo',
        humantime: undefined,
        verbose: undefined
      }

      // Act
      finalConfig = h2aLib.mergeConfigs(config, argv)

      // Assert
      expect(finalConfig).to.deep.equal({
        target: 'foo',
        filter: 'foo',
        harFiles: ['foo'],
        includeAllURLs: false,
        outputFileName: 'foo',
        outputFormat: 'foo',
        useHumanTime: false,
        verbosity: 1
      });

      ///////////////////////////
      // Arrange
      config = {
        target: 'bar',
        filter: 'bar',
        harFiles: ['bar'],
        includeAllURLs: true,
        outputFileName: 'bar',
        outputFormat: 'bar',
        useHumanTime: true,
        verbosity: 1
      }

      argv = {
        target: 'foo',
        filter: 'foo',
        files: ['foo'],
        all: false,
        output: 'foo',
        format: 'foo',
        humantime: false,
        verbose: 0
      }

      // Act
      finalConfig = h2aLib.mergeConfigs(config, argv)

      // Assert
      expect(finalConfig).to.deep.equal({
        target: 'foo',
        filter: 'foo',
        harFiles: ['foo'],
        includeAllURLs: false,
        outputFileName: 'foo',
        outputFormat: 'foo',
        useHumanTime: false,
        verbosity: 1
      });
  	})
  })
})
