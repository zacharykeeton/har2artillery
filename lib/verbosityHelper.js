const chalk = require('chalk')

class VerbosityHelper {

    constructor(userSetVerbosity){
        this.userSetVerbosity = userSetVerbosity
    }

    writeOk(message, messageWriteAtVerbosityLevel) {


        switch (messageWriteAtVerbosityLevel) {
            case 1:
                if (this.userSetVerbosity >= 1) console.info(chalk.green(message))
                break
            case 2:
                if (this.userSetVerbosity >= 2) console.info(chalk.blue(message))
                break
            case 3:
                if (this.userSetVerbosity >= 3) console.info(chalk.gray(message))
                break
        }
    }

    // FOR SETS
    writeOkSet(set, messageWriteAtVerbosityLevel) {
        const message = JSON.stringify([...set], null, 2)
        this.writeOk(message, messageWriteAtVerbosityLevel)
    }

    writeError(message) {
        console.info(chalk.red(JSON.stringify(message)))
    }
}

module.exports = VerbosityHelper