const fs            = require('fs')
const EventEmitter  = require('events')
const YAML          = require('yamljs')

const h2aLib = require('./h2aLib')

class ArtilleryGenerator extends EventEmitter {

    loadConfigFile(configFileName) {
        let configData
        try {
            configData = fs.readFileSync(configFileName, "utf8")
            this.emit('configLoaded')
        } catch (error) {
            this.emit('error', new Error(`ERROR: Could not load config file '${configFileName}'`))
            return
        }

        let config
        try {
            config = YAML.parse(configData)
        } catch (error) {
            this.emit('error', new Error(`ERROR: Invalid config file: '${configFileName}'`))
            return
        }

        return config
    }

    buildArtilleryScript(config, argv) {

        // OVERRIDE CONFIG FILE ARGS WITH COMMAND LINE ARGS
        const { target, filter, harFiles, includeAllURLs, outputFileName, outputFormat, useHumanTime, verbosity } = h2aLib.mergeConfigs(config, argv)

        // CREATE SCENARIOS FROM FILES
        const scenarios = h2aLib.buildScenarios({ target, filter, harFiles, includeAllURLs, useHumanTime, verbosity }, this)

        const artilleryScript = {
            config: {
                target,
                phases: [{
                    name: 'PHASE 1: Warm up the application',
                    arrivalRate: 5,
                    duration: 60
                }, {
                    name: 'PHASE 2: Getting popular',
                    arrivalRate: 5,
                    rampTo: 50,
                    duration: 120
                }, {
                    name: 'PHASE 3: Sustained Bombardment',
                    arrivalRate: 50,
                    duration: 600
                }]
            },
            scenarios
        }

        return artilleryScript
    }

    writeToFile({ artilleryScript, outputFileName, outputFormat }) {
        let output
        if (outputFormat === "yaml") {
            output = YAML.stringify(artilleryScript, 7) // second arg is 'depth'. Adjust if output yml doesn't look right
            outputFileName = outputFileName + '.yml'
        } else if (outputFormat === "json") {
            output = JSON.stringify(artilleryScript, null, 2)
            ouputFileName = outputFileName + '.json'
        }
        fs.writeFileSync(outputFileName, output)
    }
}

module.exports = ArtilleryGenerator