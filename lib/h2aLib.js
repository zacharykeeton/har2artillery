const fs = require('fs')

const createScenario = ({ target, filter, harFileName, includeAllURLs, useHumanTime, verbosity }, emitter) => {

  let harFileData
  try {
    harFileData = fs.readFileSync(harFileName, "utf8")
  } catch (error) {
    emitter.emit('harError', new Error(`ERROR: Could not load har file '${harFileName}'. Skipping.`))
    return
  }

  let har
  try {
    har = JSON.parse(harFileData)
  } catch (error) {
    emitter.emit('harError', new Error(`ERROR: Invalid har file '${harFileName}'. Skipping.`))
    return
  }

  const harEntries = har.log.entries
  const flow = []

  // filter out urls to add to artillery script
  const includedEntries = harEntries.filter(
    harEntry => {
      return includeAllURLs || harEntry.request.url.startsWith(target + filter)
    }
  )
  const includedUrls = new Set(includedEntries.map( entry => entry.request.url ))

  const excludedEntries = harEntries.filter(
    harEntry => {
      return !(includeAllURLs || harEntry.request.url.startsWith(target + filter))
    }
  )

  const excludedUrls = new Set(excludedEntries.map( entry => entry.request.url ))

  for (let i = 0; i < includedEntries.length; i++) {

    const entry   = includedEntries[i]
    const method  = entry.request.method.toLowerCase()
    const url     = entry.request.url

    var flowElement = {
      [method]: { url }
    }

    if (method === 'post') {
      try {
        flowElement.json = JSON.parse(request.postData.text)
      } catch (error) {
        flowElement.body = entry.request.postData.text
      }
    }
    flow.push(flowElement)

    // Add think
    if (useHumanTime) {
      const nextEntry = includedEntries[i + 1]
      if (nextEntry !== undefined) {
        const thinkTime = getEntryStartDifference(entry, nextEntry)
        if (thinkTime > 0) flow.push({ think: thinkTime })
      }
    }
  }

  const scenario = {
    name: harFileName,
    flow
  }

  if(emitter) emitter.emit('scenarioCreated', null, { name: scenario.name, includedUrls, excludedUrls } )
  return scenario
}

const getEntryStartDifference = (entry1, entry2) => {
  const entry2Time = new Date(entry2.startedDateTime).getTime()
  const entry1Time = new Date(entry1.startedDateTime).getTime()
  const timeDiff = entry2Time - entry1Time
  return Math.round(timeDiff / 1000)
}

// EXPORTS
const mergeConfigs = (config, argv) => {
  return {
    target         : (argv && argv.target)    || config.target,
    filter         : (argv && argv.filter)    || config.filter,
    harFiles       : (argv && argv.files)     || config.harFiles,
    outputFileName : (argv && argv.output)    || config.outputFileName,
    outputFormat   : (argv && argv.format)    || config.outputFormat,
    verbosity      : (argv && argv.verbose)   || config.verbosity,
    includeAllURLs : (argv && argv.all)       === undefined ? !!config.includeAllURLs : !!argv.all,
    useHumanTime   : (argv && argv.humantime) === undefined ? !!config.useHumanTime : !!argv.humantime
  }
}

const buildScenarios = ({ target, filter, harFiles, includeAllURLs, useHumanTime, verbosity }, emitter) => {
  const scenarios = []
  for (harFileName of harFiles) {
    const scenario = createScenario({ target, filter, harFileName, includeAllURLs, useHumanTime, verbosity }, emitter)
    if(scenario) scenarios.push(scenario)
  }
  return scenarios
}

module.exports = {
  mergeConfigs,
  buildScenarios
}