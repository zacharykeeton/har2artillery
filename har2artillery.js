#!/usr/bin/env node

const yargs = require('yargs')

const VerbosityHelper     = require('./lib/verbosityHelper')
const ArtilleryGenerator  = require('./lib/ArtilleryGenerator')

const main = () => {

  // Handle command line args
  const argv = yargs
    .option('target', {
      alias: 't',
      describe: 'The artillery target',
      type: 'string',
      nargs: 1
    })
    .option('filter', {
      alias: 'q',
      describe: 'and additional url filter to narrow focus (target+filter)',
      type: 'string',
      nargs: 1
    })
    .option('files', {
      alias: 'f',
      describe: 'The HAR file(s) to consume',
      type: 'array'
    })
    .option('all', {
      alias: 'a',
      describe: 'Include ALL requested URLs in the artillery script, including 3rd party etc.',
      type: 'bool'
    })
    .option('humantime', {
      alias: 's',
      describe: 'Use the timestamps in the HAR entries to send requests at the same speed as the actual human users.',
      type: 'bool'
    })
    .option('output', {
      alias: 'o',
      describe: 'The output file for the artillery script.',
      type: 'string',
      nargs: 1
    })
    .option('format', {
      alias: 'x',
      describe: 'The output file format.',
      type: 'string',
      choices: ['yaml', 'json'],
      nargs: 1
    })
    .option('config', {
      alias: 'c',
      describe: 'The configuration file to use for this run.',
      type: 'string',
      nargs: 1,
      default: __dirname + '/config.yml'
    })
    .count('verbose')
    .alias('v', 'verbose')
    .alias('h', 'help')
    .alias('V', 'version')
    .help()
    .argv

  
  const verbosity = argv.verbose

  // Instantiate workers
  const vh        = new VerbosityHelper(verbosity)
  const generator = new ArtilleryGenerator()

  // Set event handlers
  generator.on('configLoaded', () => { 
    vh.writeOk(`CONFIG LOADED`, 2) 
  })

  generator.on('scenarioCreated', (err, scenario) => {

    // OUTPUT TO SCREEN IF VERBOSE
    vh.writeOk(`SCENARIO CREATED: ${scenario.name}`, 1)
    
    // includes
    vh.writeOk(`included URLs: ${scenario.includedUrls.size}`, 2)
    vh.writeOkSet(scenario.includedUrls, 3)

    // excludes
    vh.writeOk(`excluded URLs: ${scenario.excludedUrls.size}`, 2)
    vh.writeOkSet(scenario.excludedUrls, 3)
  
  })

  generator.on('harError', (err) => {
    vh.writeError(err.message)
  })


  generator.on('error', (err) => {
    vh.writeError(err.message)
    process.exit()
  })

  //// DO WORK

  // load config
  const config = generator.loadConfigFile(argv.config)

  // create script
  const artilleryScript = generator.buildArtilleryScript(config, argv)

  // write to file
  generator.writeToFile({ artilleryScript, outputFileName: config.outputFileName, outputFormat: config.outputFormat })
}

// EXPORTS
module.exports = ArtilleryGenerator

// RUN FOR CLI
if (require.main.filename === __filename) main()