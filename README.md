# har2artillery

> Easily convert HAR files to artillery scripts via the command-line or require().

[![Build Status](https://www.travis-ci.org/zacharykeeton/har2artillery.svg?branch=master)](https://www.travis-ci.org/zacharykeeton/har2artillery)

`npm install -g har2artillery` to use as a cli tool.

`npm install -P har2artillery` to use with require().

## Why?

> Creating realistic [artillery](https://artillery.io/) scenarios manually can be a painful process usually involving hours of url gathering and hand-jamming, but it's extremely important to ensure the viablity of our systems. We can save hours and jumpstart this process by utilizing HAR files exported by browsers. You would plan a user scenario, click through your site manually with the 'Developer Tools' open to the Network Tab (with persist logs turned on). After you manually go through your scenario, pause the Network tab and save all the network entries as a HAR file.  Repeat this for as many scenarios as you need (one HAR file per scenario). Then use this tool (har2artillery) to generate a first-pass artillery script that you can then modify to your heart's desire. With har2artillery, you will be able to create the most lifelike artillery scenarios in the least amount of time.  Use har2artillery and all your wildest dreams will come true.

## Usage

### As a Command Line Tool

#### Quickstart user story: 
1. `npm install -g har2artillery` 
2. cd into a folder of HAR files 
3. create a config.yml file in that directory (see below. Can skip the 'harFiles' setting, can use -f glob [next step])
4. `har2artillery -c ./config.yml -f *.har -o ./newScript -t yaml`
5. `cat ./newScript.yml`

#### All Options
`har2artillery --help`

```
Options:
  --target, -t     The artillery target                                   [string]
  --files, -f      The HAR file(s) to consume                              [array]
  --filter, -q     and additional url filter to narrow focus (target+filter) [string]
  --all, -a        Include ALL requested URLs in the artillery script, including
                   3rd party etc.
  --humantime, -s  Use the timestamps in the HAR entries to send requests at the
                   same speed as the actual human users.
  --output, -o     The output file for the artillery script.              [string]
  --format, -x     The output file format.      [string] [choices: "yaml", "json"]
  --config, -c     The configuration file to use for this run.
                                              [string] [default: "./config.yml"]
  -h, --help       Show help                                             [boolean]
  -v, --verbose    Set the verbosity level. Supports up to -vvv.
  -V, --version    Show version number                                   [boolean]
```

NOTE: There is a default config file loaded first, then your command line args override the values
you specify.  To see/change this default config, see har2artillery/config.yml  (see below)

#### On the command line via config file
You can also make a config file and do `har2artillery -c ./path/to/your/config`

This will totally override the default config file loaded when using har2artillery on the command line.

The config file format is like so:
```
---
target: 'http://example.com'
filter: ''
includeAllURLs: false
harFiles: 
  - './example.har'
outputFileName: './example_artillery_script'
outputFormat: 'yaml'
useHumanTime: false
verbosity: 0
```

#### The default config file
Find this at `har2artillery/config.yml`.  It is only loaded when you are using har2artillery as a cli tool.

```
---
# The artillery target.
# Watch your trailing slashes with 'filter' down below. Be sure they work well
# together (target+filter) eg:
# target 'http://example.com'
# filter: '/api'
target: http://example.com

# An extra string to concat with target to further narrow down urls included in
# output script.  Eg. filter: '/api'
# URLs where url.startsWith(target + filter) === true will be included.
filter: ''

# If this is set to true, all requests will be included in the output script (3rd party, etc.)
# If this is set to false, only urls where url.startsWith(target + filter) === true
# will be included.
includeAllURLs: false


# HAR (HTTP ARchive) file name. Commonly exported from a browser after a live session.
harFiles: 
  - "./example.har"

# The generated artillery script file name.
# Do not add an extension. It will be determined by the outputFormat 'json|yaml'
outputFileName: ./example_artillery_script

# The format of the generated artillery script.
# Currently supports 'yaml' and 'json'
outputFormat: yaml

# Use the timestamps in the HAR entries to send requests at the same speed as the actual human users
# when they were clicking through the site to create the HAR
useHumanTime: false

# Verbosity level. Set with -vvvv (level 4), etc.
verbosity: 0
```
### In your code
`npm install --save har2artillery`

```
const Har2Artillery = require('./har2artillery')
const h2a = new Har2Artillery()

// [OPTIONAL] Set event handlers
h2a.on('configLoaded', () => {
    console.log(`CONFIG LOADED`, 2)
})

h2a.on('scenarioCreated', (err, scenario) => {

    console.log(`SCENARIO CREATED: ${scenario.name}`)

    // includes
    console.log(`included URLs: ${scenario.includedUrls.size}`)
    console.log(scenario.includedUrls)

    // excludes
    console.log(`excluded URLs: ${scenario.excludedUrls.size}`)
    console.log(scenario.excludedUrls)

})

h2a.on('harError', (err) => {
    console.error(err.message)
})


h2a.on('error', (err) => {
    console.error(err.message)
    process.exit()
})

// CONFIGURE
config = {}
config.target = 'http://www.example.com'                // The artillery target
config.filter = '/api'                                  // an extra filter to further narrow down urls included in generated script (url.startsWith(target+filter))
config.harFiles = ['./scenario1.har', './scenario2.har']  // Each har file will become a scenario
config.includeAllURLs = false                                   // false: only include urls starting with (target+filter). True include all (3rd party, etc)
config.outputFileName = 'myNewArtilleryScript'                  // no extension here
config.outputFormat = 'yaml'                                  // can be 'yaml' or 'json'. Determines file extentions (.yml|.json)
config.useHumanTime = false									// if true: use the timestamps in the HAR entries to send requests at the same speed as the actual human users (slower)
config.verbosity = 0                                       // stdout verbosity level (integer >= 0)

// DO WORK
const artilleryScript = h2a.buildArtilleryScript(config)        // Builds artillery script as javascript object

// OUTPUT
console.log(JSON.stringify(artilleryScript))

// Serialize as yaml or json and write to file
h2a.writeToFile({ artilleryScript, outputFileName: config.outputFileName, outputFormat: config.outputFormat })					
```
  
## License

WTFPL